# nx-quarkus-gradle

This library was generated with [Nx](https://nx.dev).

## Building

Run `nx build nx-quarkus-gradle` to build the library.

## Running unit tests

Run `nx test nx-quarkus-gradle` to execute the unit tests via [Jest](https://jestjs.io).
