export interface LibraryGeneratorSchema {
    name: string;
    tags?: string;
    directory?: string;
    groupId: string;
    projectVersion: string;
    projects?: string;
}
