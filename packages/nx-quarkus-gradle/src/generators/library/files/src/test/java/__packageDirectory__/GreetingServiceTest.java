package <%= packageName %>;

import static org.junit.jupiter.api.Assertions.assertEquals;

import io.quarkus.test.junit.QuarkusTest;
import javax.inject.Inject;
import org.junit.jupiter.api.Test;

@QuarkusTest
public class GreetingServiceTest {

    @Inject
    GreetingService service;
    
    @Test
    public void testGreetingService() {
        assertEquals("Hello from RESTEasy Reactive", service.greeting());
    }

}
