import {
  addProjectConfiguration,
  formatFiles,
  generateFiles,
  getWorkspaceLayout,
  names,
  offsetFromRoot, readProjectConfiguration,
  Tree,
} from '@nrwl/devkit';
import * as path from 'path';
import { LibraryGeneratorSchema } from './schema';
import { normalizeName } from '../../utils/command';

interface NormalizedSchema extends LibraryGeneratorSchema {
  projectName: string;
  projectRoot: string;
  projectDirectory: string;
  parsedTags: string[];
  packageName: string;
  packageDirectory: string;
  parsedProjects: string[];
}

function normalizeOptions(tree: Tree, options: LibraryGeneratorSchema): NormalizedSchema {
  const simpleProjectName = names(normalizeName(options.name)).fileName;
  const projectName = options.directory
    ? `${normalizeName(names(options.directory).fileName)}-${simpleProjectName}`
    : simpleProjectName;
  const projectDirectory = options.directory
    ? `${names(options.directory).fileName}/${simpleProjectName}`
    : simpleProjectName;
  const projectRoot = `${getWorkspaceLayout(tree).libsDir}/${projectDirectory}`;
  const parsedTags = options.tags
    ? options.tags.split(',').map((s) => s.trim())
    : [];
  
  const packageName = `${options.groupId}.${
    options.directory
      ? `${names(options.directory).fileName.replace(
        new RegExp(/\//, 'g'),
        '.'
      )}.${names(simpleProjectName).className.toLocaleLowerCase()}`
      : names(simpleProjectName).className.toLocaleLowerCase()
  }`;
  const packageDirectory = packageName.replace(new RegExp(/\./, 'g'), '/');
  
  const parsedProjects = options.projects
    ? options.projects.split(',').map((s) => s.trim())
    : [];

  return {
    ...options,
    projectName,
    projectRoot,
    projectDirectory,
    parsedTags,
    packageName,
    packageDirectory,
    parsedProjects,
  };
}

function addFiles(tree: Tree, options: NormalizedSchema) {
  const templateOptions = {
    ...options,
    ...names(options.name),
    offsetFromRoot: offsetFromRoot(options.projectRoot),
    template: ''
  };
  generateFiles(tree, path.join(__dirname, 'files'), options.projectRoot, templateOptions);
}

export default async function (tree: Tree, options: LibraryGeneratorSchema) {
  const normalizedOptions = normalizeOptions(tree, options);
  addProjectConfiguration(
    tree,
    normalizedOptions.projectName,
    {
      root: normalizedOptions.projectRoot,
      projectType: 'library',
      sourceRoot: `${normalizedOptions.projectRoot}/src`,
      targets: {
        build: {
          executor: "@htc-plugins/nx-quarkus-gradle:build",
        },
        lint: {
          executor: '@htc-plugins/nx-quarkus-gradle:lint',
        },
        test: {
          executor: '@htc-plugins/nx-quarkus-gradle:test',
        },
      },
      tags: normalizedOptions.parsedTags,
    }
  );
  addFiles(tree, normalizedOptions);
  addProjectToGradleSetting(tree, normalizedOptions);
  addLibraryToProjects(tree, normalizedOptions);
  await formatFiles(tree);
}

function addProjectToGradleSetting(tree: Tree, options: NormalizedSchema) {
  const filePath = `settings.gradle`;
  const ktsFilePath = `settings.gradle.kts`;
  const regex = /.*rootProject\.name.*/;
  const gradleProjectPath = options.projectRoot.replace(
    new RegExp('/', 'g'),
    ':'
  );
  
  if (tree.exists(filePath)) {
    const settingsContent = tree.read(filePath, 'utf-8');
    
    const newSettingsContent = settingsContent.replace(
      regex,
      `$&\ninclude('${gradleProjectPath}')`
    );
    tree.write(filePath, newSettingsContent);
  }
  
  if (tree.exists(ktsFilePath)) {
    const settingsContent = tree.read(ktsFilePath, 'utf-8');
    
    const newSettingsContent = settingsContent.replace(
      regex,
      `$&\ninclude("${gradleProjectPath}")`
    );
    tree.write(ktsFilePath, newSettingsContent);
  }
}

function addLibraryToProjects(tree: Tree, options: NormalizedSchema) {
  const regex = /dependencies\s*{/;
  const gradleProjectPath = options.projectRoot.replace(
    new RegExp('/', 'g'),
    ':'
  );
  for (const projectName of options.parsedProjects) {
    const projectRoot = readProjectConfiguration(tree, projectName).root;
    const filePath = path.join(projectRoot, `build.gradle`);
    const ktsPath = path.join(projectRoot, `build.gradle.kts`);
    
    if (tree.exists(filePath)) {
      const buildGradleContent = tree.read(filePath, 'utf-8');
      const newBuildGradleContent = buildGradleContent.replace(
        regex,
        `$&\nimplementation project(':${gradleProjectPath}')`
      );
      tree.write(filePath, newBuildGradleContent);
    }
    
    if (tree.exists(ktsPath)) {
      const buildGradleContent = tree.read(ktsPath, 'utf-8');
      
      const newBuildGradleContent = buildGradleContent.replace(
        regex,
        `$&\nimplementation(project(":${gradleProjectPath}"))`
      );
      tree.write(ktsPath, newBuildGradleContent);
    }
  }
}
