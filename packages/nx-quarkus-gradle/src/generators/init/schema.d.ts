export interface NxQuarkusGradleGeneratorSchema {
  javaVersion: string | number;
  rootProjectName: string;
}
