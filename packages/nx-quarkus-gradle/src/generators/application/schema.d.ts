export interface ApplicationGeneratorSchema {
    name: string;
    tags?: string;
    directory?: string;
    groupId: string;
    projectVersion: string;
    configFormat: '.properties' | '.yml';
}
