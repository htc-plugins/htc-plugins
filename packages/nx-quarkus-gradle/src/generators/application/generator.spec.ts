import { createTreeWithEmptyWorkspace } from '@nrwl/devkit/testing';
import { Tree, readProjectConfiguration } from '@nrwl/devkit';
import generator from './generator';
import { ApplicationGeneratorSchema } from './schema';

describe('application generator', () => {
  let appTree: Tree;
  const options: ApplicationGeneratorSchema = {
    name: 'test',
    groupId: 'com.example',
    projectVersion: '0.0.1-SNAPSHOT',
    configFormat: '.yml',
  };

  beforeEach(() => {
    appTree = createTreeWithEmptyWorkspace();
    appTree.write(
      './settings.gradle',
      "rootProject.name = 'test-quarkus-multi-project'"
    );
  });

  it('should run successfully', async () => {
    await generator(appTree, options);
    const config = readProjectConfiguration(appTree, 'test');
    const ymlFile = appTree.exists(
      './apps/test/src/main/resources/application.yml'
    );
    
    expect(config).toBeDefined();
    expect(ymlFile).toBeTruthy();
  })
});
