import { ListExtensionsExecutorSchema } from './schema';
import { ExecutorContext, logger } from '@nrwl/devkit';
import { getExecutable, getProjectPath, runCommand } from '../../utils/command';

export default async function runExecutor(
  options: ListExtensionsExecutorSchema,
  context: ExecutorContext
) {
  logger.info(`Executor ran for ListExtensions: ${JSON.stringify(options)}`);
  const args = (options.args || []).join(' ');
  
  return runCommand(`${getExecutable()} ${getProjectPath(context)}:listExtensions ${args}`);
}

