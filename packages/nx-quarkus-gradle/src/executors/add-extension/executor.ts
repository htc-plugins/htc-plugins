import { AddExtensionExecutorSchema } from './schema';
import { ExecutorContext, logger } from '@nrwl/devkit';
import { getExecutable, getProjectPath, runCommand } from '../../utils/command';

export default async function runExecutor(
  options: AddExtensionExecutorSchema,
  context: ExecutorContext
) {
  logger.info(`Executor ran for AddExtension: ${JSON.stringify(options)}`);
  const args = (options.args || []).join(' ');
  
  return runCommand(`${getExecutable()} ${getProjectPath(context)}:addExtension ${args}`);
}

