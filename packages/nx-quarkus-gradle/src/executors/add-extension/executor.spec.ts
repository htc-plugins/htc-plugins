import { AddExtensionExecutorSchema } from './schema';
import executor from './executor';
import { ExecutorContext } from '@nrwl/devkit';
import { runCommand } from '../../utils/command';

jest.mock('../../utils/command');

const options: AddExtensionExecutorSchema = {};
const context: ExecutorContext = {
  root: '/root',
  cwd: '/root',
  projectName: 'my-app',
  targetName: 'add-extension',
  workspace: {
    version: 2,
    projects: {
      'my-app': {
        root: 'apps/wibble',
        sourceRoot: 'apps/wibble',
      },
    },
    npmScope: 'test',
  },
  isVerbose: false,
};

describe('AddExtension Executor', () => {
  beforeEach(async () => {
    (runCommand as jest.Mock).mockReturnValue({ success: true });
  });
  
  it('can run', async () => {
    const output = await executor(options, context);
    
    expect(output.success).toBe(true);
  });
});
