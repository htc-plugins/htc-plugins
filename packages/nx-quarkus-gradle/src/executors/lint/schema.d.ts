export interface LintExecutorSchema {
  format?: boolean;
  args?: string[];
}
