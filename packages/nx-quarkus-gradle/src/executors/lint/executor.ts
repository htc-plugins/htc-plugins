import { LintExecutorSchema } from './schema';
import { ExecutorContext, logger } from '@nrwl/devkit';
import { getExecutable, getProjectPath, runCommand } from '../../utils/command';

export default async function runExecutor(
  options: LintExecutorSchema,
  context: ExecutorContext
) {
  logger.info(`Executor ran for Lint: ${JSON.stringify(options)}`);
  const args = (options.args || []).join(' ');
  
  if (options.format) {
    return runCommand(`${getExecutable()} ${getProjectPath(context)}:spotlessApply ${args}`);
  }
  
  return runCommand(`${getExecutable()} ${getProjectPath(context)}:spotlessCheck ${args}`);
}

