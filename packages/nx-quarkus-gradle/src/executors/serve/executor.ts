import { ServeExecutorSchema } from './schema';
import { ExecutorContext, logger } from '@nrwl/devkit';
import { getExecutable, getProjectPath, runCommand, waitForever } from '../../utils/command';

export default async function runExecutor(
  options: ServeExecutorSchema,
  context: ExecutorContext
) {
  logger.info(`Executor ran for Serve: ${JSON.stringify(options)}`);
  const args = (options.args || []).join(' ');
  
  let result;
  if (options.remoteDev) {
    result = runCommand(`${getExecutable()} ${getProjectPath(context)}:quarkusRemoteDev ${args}`);
  } else {
    result = runCommand(`${getExecutable()} ${getProjectPath(context)}:quarkusDev ${args}`);
  }
  
  if (!result.success) {
    return {
      success: false,
    };
  }
  
  await waitForever();
  
  return {
    success: true,
  };
}

