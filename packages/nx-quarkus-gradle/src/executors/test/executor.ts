import { TestExecutorSchema } from './schema';
import { ExecutorContext, logger } from '@nrwl/devkit';
import { getExecutable, getProjectPath, runCommand } from '../../utils/command';

export default async function runExecutor(
  options: TestExecutorSchema,
  context: ExecutorContext
) {
  logger.info(`Executor ran for Test: ${JSON.stringify(options)}`);
  const args = (options.args || []).join(' ');
  
  return runCommand(`${getExecutable()} ${getProjectPath(context)}:test ${args}`);
}

