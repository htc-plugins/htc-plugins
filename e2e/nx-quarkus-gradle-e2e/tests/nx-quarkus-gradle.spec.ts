import {
  checkFilesExist,
  ensureNxProject, readFile,
  readJson,
  runNxCommandAsync, tmpProjPath,
  uniq, updateFile,
} from '@nrwl/nx-plugin/testing';
import { names } from '@nrwl/devkit';
import * as path from 'path';
import * as fse from 'fs-extra';

describe('nx-quarkus-gradle e2e', () => {
  // Setting up individual workspaces per
  // test can cause e2e runs to take a long time.
  // For this reason, we recommend each suite only
  // consumes 1 workspace. The tests should each operate
  // on a unique project in the workspace, such that they
  // are not dependant on one another.
  beforeAll(() => {
    ensureNxProject(
      '@htc-plugins/nx-quarkus-gradle',
      'dist/packages/nx-quarkus-gradle'
    );
  });

  afterAll(() => {
    // `nx reset` kills the daemon, and performs
    // some work which can help clean up e2e leftovers
    runNxCommandAsync('reset');
  });

  it('should init nx-quarkus-gradle with default options', async () => {
    await runNxCommandAsync(
      `generate @htc-plugins/nx-quarkus-gradle:init`
    );
    
    const packageJson = readJson('package.json');
    expect(packageJson.devDependencies['@htc-plugins/nx-quarkus-gradle']).toBeTruthy();
  
    const nxJson = readJson('nx.json');
    expect(nxJson.plugins.includes('@htc-plugins/nx-quarkus-gradle')).toBeTruthy();
  
    expect(() =>
      checkFilesExist(
        'gradle/wrapper/gradle-wrapper.jar',
        'gradle/wrapper/gradle-wrapper.properties',
        'gradlew',
        'gradlew.bat',
        'gradle.properties',
        'settings.gradle'
      )
    ).not.toThrow();
  }, 120000);
  
  it('should create a quarkus application', async () => {
    const appName = uniq('quarkus-gradle-app-');
    
    await runNxCommandAsync(`generate @htc-plugins/nx-quarkus-gradle:init`);
    
    await runNxCommandAsync(
      `generate @htc-plugins/nx-quarkus-gradle:application ${appName}`
    );
    
    expect(() =>
      checkFilesExist(
        `apps/${appName}/build.gradle`,
        `apps/${appName}/src/main/resources/application.properties`,
        `apps/${appName}/src/main/java/com/example/${names(
          appName
        ).className.toLocaleLowerCase()}/GreetingResource.java`,
        `apps/${appName}/src/test/java/com/example/${names(
          appName
        ).className.toLocaleLowerCase()}/GreetingResourceTest.java`,
        `apps/${appName}/src/native-test/java/com/example/${names(
          appName
        ).className.toLowerCase()}/GreetingResourceIT.java`
      )
    ).not.toThrow();
    
    // Making sure the build.gradle file contains the good information
    const buildGradle = readFile(`apps/${appName}/build.gradle`);
    expect(buildGradle.includes('com.example')).toBeTruthy();
    expect(buildGradle.includes('0.0.1-SNAPSHOT')).toBeTruthy();
  
    const lintResult = await runNxCommandAsync(`lint ${appName}`);
    expect(lintResult.stdout).toContain('Executor ran for Lint');
    
    const buildResult = await runNxCommandAsync(`build ${appName}`);
    expect(buildResult.stdout).toContain('Executor ran for Build');
    
    //should recreate build folder
    const localTmpDir = path.dirname(tmpProjPath());
    const targetDir = path.join(localTmpDir, 'proj', 'apps', appName, 'build');
    fse.removeSync(targetDir);
    expect(() => checkFilesExist(`apps/${appName}/build`)).toThrow();
    
    await runNxCommandAsync(`build ${appName}`);
    expect(() => checkFilesExist(`apps/${appName}/build`)).not.toThrow();
    
    const testResult = await runNxCommandAsync(`test ${appName}`);
    expect(testResult.stdout).toContain('Executor ran for Test');
  }, 1200000);
  
  it('should use specified options to create an application', async () => {
    const randomName = uniq('quarkus-gradle-app-');
    const appDir = 'deep/subdir';
    const appName = `${normalizeName(appDir)}-${randomName}`;
    
    await runNxCommandAsync(
      `generate @htc-plugins/nx-quarkus-gradle:init --dsl kotlin`
    );
    
    await runNxCommandAsync(
      `generate @htc-plugins/nx-quarkus-gradle:application ${randomName} --tags e2etag,e2ePackage --directory ${appDir} --groupId com.company --projectVersion 1.2.3 --configFormat .yml`
    );
    
    expect(() =>
      checkFilesExist(
        `apps/${appDir}/${randomName}/build.gradle`,
        `apps/${appDir}/${randomName}/src/main/resources/application.yml`,
        `apps/${appDir}/${randomName}/src/main/java/com/company/deep/subdir/${names(
          randomName
        ).className.toLocaleLowerCase()}/GreetingResource.java`,
        `apps/${appDir}/${randomName}/src/test/java/com/company/deep/subdir/${names(
          randomName
        ).className.toLocaleLowerCase()}/GreetingResourceTest.java`,
        `apps/${appDir}/${randomName}/src/native-test/java/com/company/deep/subdir/${names(
          randomName
        ).className.toLowerCase()}/GreetingResourceIT.java`
      )
    ).not.toThrow();
    
    // Making sure the build.gradle file contains the good informations
    const buildGradle = readFile(`apps/${appDir}/${randomName}/build.gradle`);
    expect(buildGradle.includes('com.company')).toBeTruthy();
    expect(buildGradle.includes('1.2.3')).toBeTruthy();
    
    //should add tags to project.json
    const projectJson = readJson(`apps/${appDir}/${randomName}/project.json`);
    expect(projectJson.tags).toEqual(['e2etag', 'e2ePackage']);
  
    const lintResult = await runNxCommandAsync(`lint ${appName}`);
    expect(lintResult.stdout).toContain('Executor ran for Lint');
    
    const buildResult = await runNxCommandAsync(`build ${appName}`);
    expect(buildResult.stdout).toContain('Executor ran for Build');
    
    const testResult = await runNxCommandAsync(`test ${appName}`);
    expect(testResult.stdout).toContain('Executor ran for Test');
  }, 1200000);
  
  it('should create a library with the specified properties', async () => {
    const randomName = uniq('quarkus-gradle-lib-');
    const libDir = 'deep/subdir';
    const libName = `${normalizeName(libDir)}-${randomName}`;
    
    await runNxCommandAsync(
      `generate @htc-plugins/nx-quarkus-gradle:init --dsl kotlin`
    );
    
    await runNxCommandAsync(
      `generate @htc-plugins/nx-quarkus-gradle:library ${randomName} --directory ${libDir} --tags e2etag,e2ePackage --groupId com.company --projectVersion 1.2.3`
    );
    
    expect(() =>
      checkFilesExist(
        `libs/${libDir}/${randomName}/build.gradle`,
        `libs/${libDir}/${randomName}/src/main/java/com/company/deep/subdir/${names(
          randomName
        ).className.toLocaleLowerCase()}/GreetingService.java`,
        `libs/${libDir}/${randomName}/src/test/java/com/company/deep/subdir/${names(
          randomName
        ).className.toLocaleLowerCase()}/GreetingServiceTest.java`
      )
    ).not.toThrow();
    
    // Making sure the build.gradle file contains the good information
    const buildGradle = readFile(`libs/${libDir}/${randomName}/build.gradle`);
    expect(buildGradle.includes('com.company')).toBeTruthy();
    expect(buildGradle.includes('1.2.3')).toBeTruthy();
    
    //should add tags to project.json
    const projectJson = readJson(`libs/${libDir}/${randomName}/project.json`);
    expect(projectJson.tags).toEqual(['e2etag', 'e2ePackage']);
  
    const lintResult = await runNxCommandAsync(`lint ${libName}`);
    expect(lintResult.stdout).toContain('Executor ran for Lint');
    
    const buildResult = await runNxCommandAsync(`build ${libName}`);
    expect(buildResult.stdout).toContain('Executor ran for Build');
    
    const testResult = await runNxCommandAsync(`test ${libName}`);
    expect(testResult.stdout).toContain('Executor ran for Test');
  }, 1200000);
  
  it('should create lib with aliases', async () => {
    const randomName = uniq('quarkus-gradle-lib-');
    const libDir = 'subdir';
    const libName = `${libDir}-${randomName}`;
    
    await runNxCommandAsync(`g @htc-plugins/nx-quarkus-gradle:init`);
    
    await runNxCommandAsync(
      `g @htc-plugins/nx-quarkus-gradle:lib ${randomName} --directory ${libDir} -t e2etag,e2ePackage --groupId com.company -v 1.2.3`
    );
    
    expect(() =>
      checkFilesExist(
        `libs/${libDir}/${randomName}/build.gradle`,
        `libs/${libDir}/${randomName}/src/main/java/com/company/subdir/${names(
          randomName
        ).className.toLocaleLowerCase()}/GreetingService.java`,
        `libs/${libDir}/${randomName}/src/test/java/com/company/subdir/${names(
          randomName
        ).className.toLocaleLowerCase()}/GreetingServiceTest.java`
      )
    ).not.toThrow();
    
    // Making sure the build.gradle file contains the good information
    const buildGradle = readFile(`libs/${libDir}/${randomName}/build.gradle`);
    expect(buildGradle.includes('com.company')).toBeTruthy();
    expect(buildGradle.includes('1.2.3')).toBeTruthy();
    
    //should add tags to project.json
    const projectJson = readJson(`libs/${libDir}/${randomName}/project.json`);
    expect(projectJson.tags).toEqual(['e2etag', 'e2ePackage']);
  
    const lintResult = await runNxCommandAsync(`lint ${libName}`);
    expect(lintResult.stdout).toContain('Executor ran for Lint');
    
    const buildResult = await runNxCommandAsync(`build ${libName}`);
    expect(buildResult.stdout).toContain('Executor ran for Build');
    
    const testResult = await runNxCommandAsync(`test ${libName}`);
    expect(testResult.stdout).toContain('Executor ran for Test');
  }, 1200000);
  
  it('should add a lib to an app dependencies', async () => {
    const appName = uniq('quarkus-gradle-app-');
    const libName = uniq('quarkus-gradle-lib-');
    
    const rootProjectName = uniq('root-project-name-');
    await runNxCommandAsync(
      `generate @htc-plugins/nx-quarkus-gradle:init --rootProjectName ${rootProjectName}`
    );
    
    await runNxCommandAsync(
      `generate @htc-plugins/nx-quarkus-gradle:application ${appName}`
    );
    
    await runNxCommandAsync(
      `generate @htc-plugins/nx-quarkus-gradle:library ${libName} --projects ${appName}`
    );
    
    // Making sure the app build.gradle file contains the lib
    const buildGradle = readFile(`apps/${appName}/build.gradle`);
    expect(buildGradle.includes(`:libs:${libName}`)).toBeTruthy();
    
    const greetingResourcePath = `apps/${appName}/src/main/java/com/example/${names(
      appName
    ).className.toLocaleLowerCase()}/GreetingResource.java`;
    const greetingResourceContent = readFile(greetingResourcePath);
    
    const regex1 = /package\s*com\.example\..*\s*;/;
    
    const regex2 = /public\s*class\s*GreetingResource\s*{/;
    
    const regex3 = /"Hello from RESTEasy Reactive"/;
    
    const newGreetingResourceContent = greetingResourceContent
      .replace(
        regex1,
        `$&\nimport javax.inject.Inject;\nimport com.example.${names(
          libName
        ).className.toLocaleLowerCase()}.GreetingService;`
      )
      .replace(regex2, '$&\n  @Inject GreetingService greetingService;')
      .replace(regex3, 'this.greetingService.greeting()');
    
    updateFile(greetingResourcePath, newGreetingResourceContent);
    
    const buildResult = await runNxCommandAsync(`build ${appName}`);
    expect(buildResult.stdout).toContain('Executor ran for Build');
    
    const testResult = await runNxCommandAsync(`test ${appName}`);
    expect(testResult.stdout).toContain('Executor ran for Test');
    
    await runNxCommandAsync(`dep-graph --file=output.json`);
  }, 1200000);
  
  function normalizeName(name: string) {
    return name.replace(/[^0-9a-zA-Z]/g, '-');
  }
});
